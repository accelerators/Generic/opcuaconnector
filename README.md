# Opcua

A Tango class generic generic gateway attempt for opcua.

## Cloning

To clone this project, type

```
git clone git@gitlab.esrf.fr:accelerators/Generic/opcuaconnector.git
```

## Building and Installation

### Dependencies

The project has the following general dependencies.

* Minimum python version: 3.7
* pytango
* freeopcua https://github.com/FreeOpcUa/python-opcua
