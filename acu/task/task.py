#!/usr/bin/env python

##----------------------------------------------------------------------------
## THE ESRF pyACU LIBRARY
##----------------------------------------------------------------------------
##
## Copyright (c) 2022-EOT ESRF
## All rights reserved. This program and the accompanying materials
## are made available under the terms of the GNU Lesser Public License v3
## which accompanies this distribution, and is available at
## http:##www.gnu.org/licenses/lgpl.html
##
##
## The pyACU library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
## Public License for more details.
##
## See COPYING file for license details
##-----------------------------------------------------------------------------

"""This module defines the pyACU.Task class: a thread to which you can post messages"""

import copy
import ctypes
import queue
import threading
import time
from contextlib import contextmanager

from acu.task.message import Message, MessageInvalidIdentifier, MessageIsNotWaitable, MessageProcessingTimeout


# ===========================================================================
class FsTaskKilled(Exception):
    pass

# ===========================================================================
@contextmanager
def silent_catch(*args):
    types = args or BaseException
    try:
        yield
    except types:
        pass
    
# ===========================================================================
class Task(threading.Thread):
    """
    The pyYat active object.
    This class is supposed to be derived (useless otherwise).
    The child class should implement the following member functions:
    * on_init()
    * on_exit()
    * handle_message(fs.utils.task.Message.Message)
    * handle_periodic_message()
    """

    def __init__(self, task_name, msgq_wm=1024):
        """
        Constructor.
        @param task_name the task's name.
        @param msgq_wm message queue water mark (i.e. messages queue depth - defaults to 1024 Messages).
        """
        threading.Thread.__init__(self)
        # task name
        self._task_name = task_name
        # initialize the associated message queue
        self._msgq = queue.PriorityQueue(msgq_wm)
        # periodic message period
        self._periodic_msg_enabled = False
        self._periodic_msg_period_in_secs = 1.0
        # pending message (used to avoid periodic message starvation)
        self._pending_msg = None
        # was the last processed msg a periodic message?
        self._last_msg_was_a_periodic_msg = False
        # last periodic message timestamp
        self._last_periodic_msg_timestamp = time.time()
        # running flag
        self._has_been_successfully_started = False
        # thread uid
        self._thread_uid = 0

    @property
    def task_name(self):
        return self._task_name

    @property
    def thread_name(self):
        return self.name

    def handle_message(self, msg):
        raise NotImplementedError

    def enable_periodic_message(self, period_in_sec=1.0):
        """
        Enables the periodic messages.
        The child class MUST implement the <handle_periodic_message> function member.
        @param period_in_sec The periodic message  period in seconds (defaults to 1)
        """
        if float(period_in_sec) > 0.:
            self._periodic_msg_enabled = True
            self._periodic_msg_period_in_secs = period_in_sec

    def disable_periodic_message(self):
        """
        Disables the periodic messages.
        """
        self._periodic_msg_enabled = False

    def periodic_message_enabled(self):
        """
        Returns True if the periodic message mechanism is enabled, returns False otherwise
        """
        return self._periodic_msg_enabled

    @property
    def periodic_message_period(self):
        """
        Returns the periodic message period in seconds
        """
        return self._periodic_msg_period_in_secs

    @periodic_message_period.setter
    def periodic_message_period(self, period_in_sec):
        """
        Sets the periodic message period in seconds
        """
        self.enable_periodic_message(period_in_sec)

    def start(self):
        """
        Starts the Task synchronously (i.e. waits for the 'on_init' function to return).
        Must be there in order to override threading.Thread.Start properly.
        """
        self.start_synchronously()

    def start_synchronously(self, tmo_sec=30.0):
        """
        Starts the Task synchronously (i.e. waits for the 'on_init' function to return).
        """
        threading.Thread.start(self)
        if self.is_alive():
            self._has_been_successfully_started = True
        self.__wait_message_processed(Message.MSG_ID_INIT, timeout=tmo_sec)

    def start_asynchronously(self):
        """
        Starts the Task asynchronously (i.e. does NOT wait for the 'on_init' function to return).
        """
        threading.Thread.start(self)
        if self.is_alive():
            self._has_been_successfully_started = True
        self.__post(Message.MSG_ID_INIT)

    def kill(self):
        """
        Tries to properly kill the underlying thread
        """
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(self._thread_uid, ctypes.py_object(FsTaskKilled))
        if res == 0:
            raise ValueError("invalid thread identifier specified!")
        elif res != 1:
            # oops! we're are in trouble,
            # we have to to revert the effect of the first call to 'PyThreadState_SetAsyncExc'
            ctypes.pythonapi.PyThreadState_SetAsyncExc(self._thread_uid, 0)
            raise RuntimeError("ctypes.pythonapi.PyThreadState_SetAsyncExc failed!")

    def exit(self):
        """
        Asks the Task to exit then join with the underlying thread.
        """
        if self._has_been_successfully_started:
            self.__post(Message.MSG_ID_EXIT)
            self.join()
            self._has_been_successfully_started = False

    def post(self, msg_id, msg_data=None, msg_priority=Message.MSG_DEFAULT_PRIORITY):
        """
        Posts a Message to the Task.
        @param msg_id the identifier of the Message to be posted.
        @param msg_data the data to be attached to the Message to be posted.
        @param msg_priority the Message priority (see Message for details).
        """
        # negative msg. id. are reserved for task's internal cooking
        if msg_id < int(0):
            raise MessageInvalidIdentifier()
        self.__post(msg_id, msg_data, msg_priority)

    def post_message(self, msg):
        """
        Posts the specified Message to the Task.
        @param msg of the Message to be posted.
        """
        # insert the message into the queue
        self._msgq.put((msg.priority, msg))
        # yield execution to another thread (in the hope that it will be the consumer)
        time.sleep(.000001)

    def wait_message_processed(self, msg_id, msg_data=None, msg_priority=Message.MSG_DEFAULT_PRIORITY, timeout=3.0):
        """
        Posts a Message to the Task then wait up to <timeout> seconds for the message to be processed.
        @param msg_id the identifier of the Message to be posted.
        @param msg_data the data to be attached to the Message to be posted.
        @param msg_priority the Message priority (see Message for details).
        @param timeout the amount of time the caller accepts to wait for the message to be processed (in seconds)
        """
        # negative msg. id. are reserved for task's internal cooking
        if msg_id < int(0):
            raise MessageInvalidIdentifier()
        # wait for the message to be processed (might raise an exception in case tmo expires)
        self.__wait_message_processed(msg_id, msg_data, msg_priority, timeout)

    def wait_message_reply(self, msg, timeout=3.0):
        """
        Posts a Message to the Task then wait up to <timeout> seconds for the a reply.
        @param msg of the Message to be posted.
        @param timeout the amount of time the caller accepts to wait for the message to be processed (in seconds)
        return msg.data (might be None)
        """
        # be sure the message is waitable
        if not msg.is_waitable():
            raise MessageIsNotWaitable()
        # insert the message into the queue
        self._msgq.put((msg.priority, msg))
        # wait for the message to be processed (might raise an exception in case tmo expires)
        msg.wait_processed(timeout)
        # return data attached to the message (might be None)
        return msg.data

    def run(self):
        """
        The Task's entry point. Not supposed to be called directly (see threading.Thread.start).
        """
        # almost infinite thread loop control flag
        go_on = True
        # thread uid
        self._thread_uid = threading.current_thread().ident
        # enter main loop...
        while go_on:
            try:
                if self._pending_msg is None:
                    tmo = 1.0
                    if self._periodic_msg_enabled:
                        tmo = self._periodic_msg_period_in_secs
                    while self._pending_msg is None:
                        self._pending_msg = self._msgq.get(block=True, timeout=tmo)[1]
                if not self._pending_msg.is_ctrl_message() and self._its_time_to_process_a_periodic_msg():
                    with silent_catch():
                        # call user defined <handle_periodic_message>
                        self.handle_periodic_message()
                    self.__periodic_msg_processed()
                else:
                    try:
                        if not self._pending_msg.is_ctrl_message():
                            self.handle_message(self._pending_msg)
                        elif self._pending_msg.identifier == Message.MSG_ID_INIT:
                            self.on_init()
                        elif self._pending_msg.identifier == Message.MSG_ID_EXIT:
                            go_on = False
                            self.on_exit()
                    except BaseException as e:
                        self._pending_msg.error = e
                    finally:
                        self.__pending_msg_processed()
                        self._pending_msg = None
            except queue.Empty:
                # call to <self._msgq.get> timed out
                if self._periodic_msg_enabled:
                    with silent_catch():
                        # call user defined <handle_periodic_message>
                        self.handle_periodic_message()
                    self.__periodic_msg_processed()
            except BaseException:
                pass

    def on_init(self):
        """ Task's <on_init> default (i.e. empty) implementation"""
        print("Task::{} warning default <on_init> implementation called!".format(self.name))

    def on_exit(self):
        """ Task's <on_exit> default (i.e. empty) implementation"""
        print("Task::{} warning default <on_exit> implementation called!".format(self.name))

    def handle_periodic_message(self):
        """ Task's <handle_periodic_message> default (i.e. empty) implementation"""
        print("Task::{} warning default <handle_periodic_message> implementation called!".format(self.name))

    def _its_time_to_process_a_periodic_msg(self):
        """ This is private and undocumented! """
        if self._periodic_msg_enabled and not self._last_msg_was_a_periodic_msg:
            dt = time.time() - self._last_periodic_msg_timestamp
            if dt >= self._periodic_msg_period_in_secs:
                return True
        return False

    def __post(self, msg_id, msg_data=None, msg_priority=Message.MSG_DEFAULT_PRIORITY):
        # instantiate the message
        msg = Message(int(msg_id), msg_data, msg_priority)
        # insert the message into the queue
        self._msgq.put((int(msg.priority), msg))
        # yield execution to another thread (in the hope that it will be the consumer)
        time.sleep(.000001)

    def __wait_message_processed(self, msg_id, msg_data=None, msg_priority=Message.MSG_DEFAULT_PRIORITY, timeout=3.0):
        # instantiate the message
        msg = Message(msg_id, msg_data, msg_priority, True)
        # insert the message into the queue
        self._msgq.put((msg.priority, msg))
        # wait for the message to be processed (might raise an exception in case tmo expires)
        msg.wait_processed(timeout)

    def __pending_msg_processed(self):
        """ This is private and undocumented! """
        self._pending_msg.processed()
        self._pending_msg = None
        self._last_msg_was_a_periodic_msg = False
        self._msgq.task_done()

    def __periodic_msg_processed(self):
        self._last_msg_was_a_periodic_msg = True
        self._last_periodic_msg_timestamp = time.time()
        






##----------------------------------------------------------------------------------------------------
## Task usage example: MyTask (does not demonstrate the best way to use Task - just for test purpose)
##----------------------------------------------------------------------------------------------------
class MyError(Exception):
    "MyTask error"
    pass

##----------------------------------------------------------------------------------------------------
## Task usage example: MyTask (does not demonstrate the best way to use Task - just for test purpose)
##----------------------------------------------------------------------------------------------------
class MyTask(Task): 
    """
    Task usage example.
    """ 
    
    ## some user defined msg identifiers 
    ## msg IDs should be exposed this way, anyway that's just for test purpose
    ASYNCH_MSG_ID     = 0
    FAST_SYNCH_MSG_ID = 1
    SLOW_SYNCH_MSG_ID = 2 
    PROC_ERROR_MSG_ID = 3  
    GET_DATA_MSG_ID   = 4 
    
    def __init__(self):
        """
        Constructor.
        """
        Task.__init__(self, "MyTask", 5)
        ## msg counter
        self._msg_cnt = 0
        ## periodic msg counter
        self._pmsg_cnt = 0
        ## data protection
        self._lock = threading.Lock()
        ## data produced by the task
        self._mydata = list()
        ## state and status 
        self._state = 0 ##tango.ON
        self._status = "Thread is up and running"

    @property
    def mydata(self):
        ## critical section to safely return our data 
        with self._lock:
            return copy.deepcopy(self._mydata)
        
    @property
    def state(self):
        with self._lock:
            return copy(self._state)

    @property
    def status(self):
        with self._lock:
            return copy(self._status)

    def on_init (self):
        """
        This is called at Task startup in the context of the underlying thread.
        """ 
        print('pyYat.Task::{} on_init called!'.format(self.name))
    
    def on_exit (self):
        """
        This is called at Task exit in the context of the underlying thread.
        """ 
        print('pyYat.Task::{} on_exit called!'.format(self.name))
        print('pyYat.Task::{} consumed {} msgs and {} p-msgs'.format(self.name, self._msg_cnt, self._pmsg_cnt))
        
    def handle_message(self, msg):
        """
        This is called upon receipt of a new message in the context of the underlying thread.
        """ 
        self._msg_cnt += 1
        print('pyYat.Task::{} receiving a msg of type...{}'.format(self.name, msg.identifier))
        print('pyYat.Task::{} received msg priority.....{}'.format(self.name, msg.priority))
        if msg.has_data():
            print('pyYat.Task::{} received msg data.........{}'.format(self.name, msg.data))
        if msg.identifier == MyTask.SLOW_SYNCH_MSG_ID:
            time.sleep(2)
        elif msg.identifier == MyTask.PROC_ERROR_MSG_ID:
            print('pyYat.Task::{} RAISING MyError exception...'.format(self.name))
            raise MyError()
        elif msg.identifier == MyTask.GET_DATA_MSG_ID:
            print("Input data: {}".format(msg.data))
            msg.data = copy.deepcopy(self._mydata)
            
    def handle_periodic_message(self):
        """
        This is called upon receipt of a periodic message in the context of the underlying thread.
        """ 
        self._pmsg_cnt += 1
        print('pyYat.Task::{} receiving a periodic msg'.format(self.name))
        ## critical section to safely change our data 
        with self._lock:
            self._mydata.append(self._pmsg_cnt)
            print('pyYat.Task::data length is {}'.format(len(self._mydata)))

    def get_data(self):
        print('t.get_data: waiting for the task to return its data...')
        m = Message(MyTask.GET_DATA_MSG_ID, msg_data="Hello Word", waitable=True)
        self.wait_message_reply(m, timeout=2.0)
        return m.data

##----------------------------------------------------------------------------------------------------
## Task usage example: main (does not demonstrate the best way to use Task - just for test purpose)
##----------------------------------------------------------------------------------------------------
def main():
    ## instanciate MyTask
    t = MyTask()
    ## enable periodic messages (one msg every half seconds)
    t.enable_periodic_message(0.5)
    ## start the Task
    t.start()
    print('pyYat.Task::{} has started'.format(t.name))
    ## asynchronous msg processing (post then go) 
    for i in range (0, 10):
        ## here we play with msg data
        t.post(MyTask.ASYNCH_MSG_ID, 2 * i)
        ## give the task some time to consume msgs 
        ## this is just for test puropose (slowdown to see periodic msgs)
        time.sleep(0.5)
        d = t.mydata
        print('pyYat.Task::length of deepcopy of data {}'.format(len(d)))
    ## synchronous msg processing (wait msg to be handled) 
    try:
        print('waiting for FAST_SYNCH_MSG_ID message to be processed...')
        t.wait_message_processed(MyTask.FAST_SYNCH_MSG_ID, timeout=1.0)
        print('t.wait_message_processed returned without raising any exception')
    except MessageProcessingTimeout:
        print('t.wait_message_processed raised a timeout exception')  
    ## synchronous msg processing (wait msg to be handled)  
    try:
        print('waiting for SLOW_SYNCH_MSG_ID to be processed...')
        t.wait_message_processed(MyTask.SLOW_SYNCH_MSG_ID, timeout=1.0)
        print('t.wait_message_processed returned without raising any exception')
    except MessageProcessingTimeout:
        print('t.wait_message_processed raised a timeout exception')
    ## synchronous msg processing (wait msg to be handled - exceptions can cross the thread bondary!)  
    try:
        print('waiting for PROC_ERROR_MSG_ID to be processed...')
        t.wait_message_processed(MyTask.PROC_ERROR_MSG_ID, timeout=2.0)
        print('t.wait_message_processed returned without raising any exception')
    except MyError:
        print('t.wait_message_processed raised a MyError exception')
    except MessageProcessingTimeout:
        print('t.wait_message_processed raised a timeout exception')
    ## synchronous data exchange - method I 
    try:
        print('t.wait_message_response: waiting for the task to return its data...')
        m = Message(MyTask.GET_DATA_MSG_ID, msg_data="Hello Word", waitable=True)
        t.wait_message_response(m, timeout=2.0)
        print('t.wait_message_response: got a list of {} elements for the Task'.format(len(m.data)))
    except MessageProcessingTimeout:
        print('t.wait_message_response raised a timeout exception')
    ## synchronous data exchange - method II (recommended)
    try:
        d = t.get_data()
        print('t.get_data: got a list of {} elements for the Task'.format(len(d)))
    except MessageProcessingTimeout:
        print('t.wait_message_response raised a timeout exception')
    ## ask the Task to exit
    t.exit()

if __name__ == '__main__':
  main()



            
