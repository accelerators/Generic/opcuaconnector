#!/usr/bin/env python

##----------------------------------------------------------------------------
## THE ESRF pyACU LIBRARY
##----------------------------------------------------------------------------
##
## Copyright (c) 2022-EOT ESRF
## All rights reserved. This program and the accompanying materials
## are made available under the terms of the GNU Lesser Public License v3
## which accompanies this distribution, and is available at
## http:##www.gnu.org/licenses/lgpl.html
##
##
## The pyACU library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
## Public License for more details.
##
## See COPYING file for license details
##-----------------------------------------------------------------------------

"""LogAdapter"""

from __future__ import print_function

import time
from contextlib import wraps


def tracer(fn):

    def printer(obj, message):
        try:
            obj.logger.debug(message)
        except Exception:
            print(message)

    @wraps(fn)
    def wrapper(self, *args, **kwargs):
        t0 = time.time()
        qualified_name = "{}.{}".format(self.__class__.__name__, fn.__name__)
        printer(self, "{} <<in".format(qualified_name))
        try:
            return fn(self, *args, **kwargs)
        finally:
            dt = 1000 * (time.time() - t0)
            printer(self, "{} out>> [took: {:.2f} ms]".format(qualified_name, dt))

    return wrapper


class DeviceLogAdapter(object):
    """A logger forwarding log to the tango device logging streams"""

    def __init__(self, host_device=None):
        self.host_device = host_device

    @property
    def logger(self):
        return self

    @classmethod
    def __log(cls, stream, *args):
        try:
            stream(*args)
        except Exception:
            print(*args)

    def debug(self, msg, *args):
        self.__log(self.host_device.debug_stream, msg, *args)

    def info(self, msg, *args):
        self.__log(self.host_device.info_stream, msg, *args)

    def deprecated(self, msg, *args):
        self.__log(self.host_device.info_stream, msg, *args)

    def warning(self, msg, *args):
        self.__log(self.host_device.warn_stream, msg, *args)

    def error(self, msg, *args):
        self.__log(self.host_device.error_stream, msg, *args)

    def critical(self, msg, *args):
        self.__log(self.host_device.fatal_stream, msg, *args)

    def fatal(self, msg, *args):
        self.__log(self.host_device.fatal_stream, msg, *args)

    def exception(self, msg, *args):
        self.__log(self.host_device.error_stream, msg, *args)

