"""This is a Tango Device demonstrating the capabilities of pyacu.Task"""
import time
from copy import copy
from datetime import datetime

import tango
from opcua import Client
from opcua import ua
from tango.server import Device, device_property, command, attribute, run

from acu.logging.LogAdapter import DeviceLogAdapter, tracer
from acu.task.message import Message
from acu.task.task import Task


class ReadForwardTask(Task, DeviceLogAdapter):
    # user defined messages (identifiers must be positive integer values)
    CHANGE_CTRL_LOOP_PERIOD = 1
    CMD_ENABLE_FORWARD = 2
    CMD_DISABLE_FORWARD = 3
    GET_STATE = 4
    GET_STATUS = 5
    GET_OPC_CONNECTION_STATUS = 6
    GET_OPC_ATTR_LIST = 7
    GET_OPC_VALUE = 100  # 100  to  100+199 reserved for dynamic attributes for dyn read attribute

    # timestamp with ISO format
    @staticmethod
    def now():
        return datetime.utcnow().strftime('%FT%T.%f')[:-3]

    # host_device: the SparkTemperatureController device hosting this task
    def __init__(self, host_device) -> None:
        self.__listOfNodeToRead = []
        self.__listOfNodeToWrite = []
        self.__listOfDeviceAttribute = []
        self.__list_opc_attr_name = []
        self.__host_device = None
        self.__client_opcua = None
        self.__connection_opcua_ok = False
        self.__forward_enable = False
        self.__opc_values = []

        try:
            Task.__init__(self, "ReadForwardTask")
            DeviceLogAdapter.__init__(self, host_device)
            self.__host_device = host_device
            self.__forward_enable = host_device.AutoStart

            self.__client_opcua = Client(host_device.OpcuaEndPoint)
            self.__client_opcua.connect()
            self.__connection_opcua_ok = True

            for line_forward_attribute in host_device.ForwardAttribute:
                split = line_forward_attribute.split(",")
                if len(split) != 2:
                    tango.Except.throw_exception(
                        "Forwardattribute not good formated Please format every line with"
                        " \"tangoDeviceAttribute,ns=N;i=xxxx\" current line with error is \"" + line_forward_attribute + "\"",
                        "ERROR_FORMAT_PROPERTY", "OpcuaConnector.init_device()")

                self.__listOfDeviceAttribute.append(tango.AttributeProxy(split[0]))
                node = self.__client_opcua.get_node(split[1])
                self.__listOfNodeToWrite.append(node)
                self.__listOfNodeToRead.append(node)
                self.__list_opc_attr_name.append(
                    str(node.get_parent().get_browse_name()).split(":")[1][0:-1].replace(" ", "_"))

            for opc_attribute in host_device.OpcuaIdToRead:
                if opc_attribute != "":
                    splited = opc_attribute.split(",")
                    if len(splited) != 4:
                        tango.Except.throw_exception(
                            "OpcuaEndPoint not good formated Please format every line with"
                            " \"ns=N;i=xxxx,attrName,type (DevDouble,DevLong),r or rw\" current line with error is \"" + opc_attribute + "\"",
                            "ERROR_FORMAT_PROPERTY", "OpcuaConnector.init_device()")
                    node = self.__client_opcua.get_node(splited[0])
                    self.__listOfNodeToRead.append(node)
                    self.__list_opc_attr_name.append(splited[1].replace(" ", "_"))

        except Exception as ex:
            print(ex)
            raise

    # called at Task startup (executed in the context of the underlying thread)
    @tracer
    def on_init(self):
        # enable periodic activity - period in seconds defined by device property
        self.enable_periodic_message(self.__host_device.ReadForwardTaskPeriodInSeconds)
        # connect to the fans controller device - defined by device property
        # self.__fans_ctrl_dev = tango.DeviceProxy(self.__host_device.FansControllerDevice)

    # called when Task is asked to exit (executed in the context of the underlying thread)
    @tracer
    def on_exit(self):
        self.__client_opcua.disconnect()
        pass

    # user message handling (executed in the context of the underlying thread)
    @tracer
    def handle_message(self, msg):
        if msg.identifier == ReadForwardTask.CHANGE_CTRL_LOOP_PERIOD:
            # change the control loop period (i.e. simply change the periodic message period of the task)
            # thh new value is attached to the message and can be retrieved using "msg.data"
            self.enable_periodic_message(msg.data)
        elif msg.identifier == ReadForwardTask.CMD_ENABLE_FORWARD:
            self.__forward_enable = True
        elif msg.identifier == ReadForwardTask.CMD_DISABLE_FORWARD:
            self.__forward_enable = False
        elif msg.identifier == ReadForwardTask.GET_STATE:
            if self.__forward_enable:
                if self.__connection_opcua_ok:
                    msg.data = tango.DevState.ON
                else:
                    msg.data = tango.DevState.FAULT
            else:
                msg.data = tango.DevState.DISABLE
        elif msg.identifier == ReadForwardTask.GET_STATUS:
            if self.__forward_enable:
                if self.__connection_opcua_ok:
                    msg.data = "Values is forwarding to OPCUA"
                else:
                    msg.data = "OPCUA server is not reachable"
            else:
                msg.data = "Values is forwarding is Disable"
        elif msg.identifier == ReadForwardTask.GET_OPC_CONNECTION_STATUS:
            state_opc = tango.DevState.UNKNOWN
            if self.__connection_opcua_ok:
                state_opc = tango.DevState.ON

            msg.data = copy(state_opc)
        elif msg.identifier == ReadForwardTask.GET_OPC_ATTR_LIST:
            msg.data = copy(self.__list_opc_attr_name)
        elif ReadForwardTask.GET_OPC_VALUE <= msg.identifier < (
                ReadForwardTask.GET_OPC_VALUE + len(self.__listOfNodeToRead) + len(self.__host_device.OpcuaIdToRead)):
            index = msg.identifier - ReadForwardTask.GET_OPC_VALUE
            msg.data = copy(self.__opc_values[index])
        # ----------------------------------------------
        else:
            self.warning("ControllerTask: received an unknown/unhandled msg!")

    # periodic message handling (executed in the context of the underlying thread)
    @tracer
    def handle_periodic_message(self):
        # periodic activity
        try:
            self.debug("ControllerTask: handling PERIODIC msg received on {}".format(self.now()))
            # call the control function
            self.__acquisition()
        except Exception as ex:
            print(ex)
            raise

    # the control loop function (called periodically)
    @tracer
    def __acquisition(self):
        # if not self.__enabled:
        # do nothing if temp. control is disabled
        # we could also check that the state/status of the fans controller device
        self.debug("Acquisition")

        values = []

        # Reading Tango Value
        for ap, node in zip(self.__listOfDeviceAttribute, self.__listOfNodeToRead):
            try:
                value = ap.read().value
                f = float(value)
                values.append(ua.Variant(f, ua.VariantType.Float))
            except Exception as ex:
                values.append(0.0)
                print(ex)

        # do nothing if the list is empty
        if len(self.__listOfNodeToWrite) != 0:
            try:
                # Write OPC Value if Enable
                if self.__forward_enable:
                    self.__client_opcua.set_values(self.__listOfNodeToWrite, values)
                    self.__connection_opcua_ok = True
            except Exception as ex:
                self.__connection_opcua_ok = False
                # not tested add try except if don't work
                self.__client_opcua.close_session()
                self.__client_opcua = Client(self.__host_device.OpcuaEndPoint)
                self.__client_opcua.connect()
                print(ex)

        # Reading OPC Value
        self.__opc_values = self.__client_opcua.get_values(self.__listOfNodeToRead)


# our Hqps Speed to Opcua Temperature Controller class
class OpcuaConnector(Device):
    OpcuaEndPoint = device_property(dtype=tango.DevString, mandatory=True,
                                    doc="opcua endpoint like opc.tcp://smartcheck1:4840/")

    ForwardAttribute = device_property(dtype=tango.DevVarStringArray, mandatory=False,
                                       doc="One line, One tango attribute forward to OPCUA word"
                                           "full qualified tango attribute separated by a comma and namespace and identifier of OPCUA separated by semicolon\n"
                                           "example : infra/hqps-accumulator/10a/Speed,ns=2;i=3902139283\n" \
                                           "infra/hqps-accumulator/10b/Speed,ns=2;i=1602391362\n")

    OpcuaIdToRead = device_property(dtype=tango.DevVarStringArray, mandatory=False,
                                    doc="One line, One tango attribute read from OPCUA word"
                                        "namespace and identifier of OPCUA separated by semicolon a comma and name of the attribute created localy a comma and the type only a comma and if the attribute is read or read/write (r or rw)\n"
                                        "example : ns=2;i=3902139283,value1,DevDouble,r\n" \
                                        "ns=2;i=1602391362, value2, DevLong, rw\n")

    AutoStart = device_property(dtype=bool, default_value=True)

    ReadForwardTaskPeriodInSeconds = 1.0

    # the device initialization
    def init_device(self):
        super().init_device()
        self.__lastRefresh = time.time()
        self.__read_forward_task = None
        self.__dynAttr = []
        self.__dynAttrRead = []
        if self.ForwardAttribute == None:
            self.ForwardAttribute = []

        db = tango.Database()
        datum = db.get_device_attribute_property(self.get_name(), ["acquisition_loop_period"])

        try:
            # instantiate the controller task (i.e. the thread implementing the control loop)
            self.__read_forward_task = ReadForwardTask(self)
            # start the task (tmo = 10 secs)
            self.__read_forward_task.start_synchronously(10.0)

            if "__value" in datum["acquisition_loop_period"]:
                self.ReadForwardTaskPeriodInSeconds = int(datum["acquisition_loop_period"]["__value"][0])
                self.__read_forward_task.post(ReadForwardTask.CHANGE_CTRL_LOOP_PERIOD,
                                              msg_data=self.ReadForwardTaskPeriodInSeconds)

            self.get_device_attr().get_w_attr_by_name("acquisition_loop_period").set_write_value(
                self.ReadForwardTaskPeriodInSeconds)

            # switch to our "up and running" state & status
            self.manage_state_status()

        except Exception as ex:
            # just an example
            print(ex)
            self.set_state(tango.DevState.FAULT)
            self.set_status("Oops, enable to start the controller task!")

    # the device cleanup
    def delete_device(self):
        try:
            # ask the regulation task to exit
            self.__regulation_task.exit()
            # ask the acquisition task to exit
            self.__read_forward_task.exit()
        except Exception as ex:
            self.error(ex)

    def always_executed_hook(self):
        now = time.time()
        if (now - self.__lastRefresh) > 1.0:
            self.__lastRefresh = now

            self.manage_state_status()

    def initialize_dynamic_attributes(self):
        msg = Message(ReadForwardTask.GET_OPC_ATTR_LIST, waitable=True)
        self.__read_forward_task.wait_message_reply(msg, timeout=2.0)
        list_of_opc_attr = msg.data

        for opc_attr_name in list_of_opc_attr:
            try:
                my_t = tango.Attr(opc_attr_name, tango.DevDouble, tango.READ)
                self.add_attribute(my_t, OpcuaConnector.read_opc_attr, None, None)
                self.__dynAttr.append(opc_attr_name)
            except Exception as ex:
                print(ex)

        for opc_attrName in self.OpcuaIdToRead:
            if opc_attrName != "":
                splited = opc_attrName.split(',')

                type_num = self.parsing_attr_type(splited[2])

                r_rw = tango.READ
                if splited[3] == "rw":
                    r_rw = tango.READ_WRITE

                try:
                    attrName = splited[1].replace(" ", "_")
                    my_t = tango.Attr(attrName, type_num, r_rw)
                    self.add_attribute(my_t, OpcuaConnector.read_opc_attr, None, None)
                    self.__dynAttr.append(attrName)
                except Exception as ex:
                    print(ex)

    def parsing_attr_type(self, typing):
        if typing == "DevDouble":
            return tango.DevDouble
        elif typing == "DevLong":
            return tango.DevLong
        else:
            return tango.DevString

    def read_opc_attr(self, attr) -> tango.DevDouble:
        index = 0
        try:
            index = self.__dynAttr.index(attr.get_name())
        except ValueError:
            tango.Except.throw_exception("attribute not found in dynamic attribute list",
                                         "attribute not found in dynamic attribute list",
                                         "OpcuaConnector.read_opc_attr()")

        msg = Message(ReadForwardTask.GET_OPC_VALUE + index, waitable=True)
        self.__read_forward_task.wait_message_reply(msg, timeout=2.0)
        attr.set_value(msg.data)

    @command(dtype_in=tango.DevVoid, dtype_out=tango.DevVoid)
    def enable(self) -> None:
        # post a ControllerTask.DISABLE_CTRL msg to the task
        self.__read_forward_task.post(ReadForwardTask.CMD_ENABLE_FORWARD)
        tango.Database().put_device_property(self.get_name(), {"AutoStart": True})
        self.manage_state_status()

    @command(dtype_in=tango.DevVoid, dtype_out=tango.DevVoid)
    def disable(self) -> None:
        # post a ControllerTask.DISABLE_CTRL msg to the task
        self.__read_forward_task.post(ReadForwardTask.CMD_DISABLE_FORWARD)
        tango.Database().put_device_property(self.get_name(), {"AutoStart": False})
        self.manage_state_status()

    @attribute(dtype='DevState')
    def opcua_connection_state(self) -> tango.DevState:
        msg = Message(ReadForwardTask.GET_OPC_CONNECTION_STATUS, waitable=True)
        self.__read_forward_task.wait_message_reply(msg, timeout=2.0)
        return msg.data

    @attribute(memorized=True, display_level=tango.DispLevel.EXPERT)
    def acquisition_loop_period(self) -> None:
        return self.__read_forward_task.periodic_message_period

    @acquisition_loop_period.write
    def acquisition_loop_period(self, clp: tango.DevDouble = 1000) -> tango.DevVoid:
        # ask the task to change the period of its periodic activity
        # here we simply illustrate how easy is to attach some data to a message (using the msg_data argument)
        # note that a call to  self.__control_task.periodic_message_period = clp would have done the job
        self.__read_forward_task.post(ReadForwardTask.CHANGE_CTRL_LOOP_PERIOD, msg_data=clp)

    def manage_state_status(self) -> None:
        self.set_state(tango.DevState.UNKNOWN)
        msg = Message(ReadForwardTask.GET_STATE, waitable=True)
        self.__read_forward_task.wait_message_reply(msg, timeout=2.0)
        self.set_state(msg.data)
        msg = Message(ReadForwardTask.GET_STATUS, waitable=True)
        self.__read_forward_task.wait_message_reply(msg, timeout=2.0)
        self.set_status(msg.data)


if __name__ == '__main__':
    try:
        run((OpcuaConnector,))
    except Exception as e:
        print(e)
        raise
